

/*
    Class level, Method level, and Property level decorators
    *

 ** Decorators - Unlike Python's decorator, JS decorators are used to share common code between classes/instances. 
                    ** A better alternative to inheritance.
 **** Decorators/Adapters in other languages are used as higher order functions (HOFs). 
            As JS functions are first class objects, and functions can be passed as params to other functions, HOF model
                is inherently supported.


*/

/**
 *  Method descriptor. **Supports only methods
 * @param {*} target 
 * @param {*} name 
 * @param {*} description 
 */
function methodDescriptor(target, name, descriptor) {
    console.log(target)
    console.log(name)

    let originalFunc = descriptor.value

    if(typeof originalFunc === 'function') {
        descriptor.value = (...args) => {
            console.log('In descriptor func- pre target func execution')

            try {
                return originalFunc.apply(this, args)
            }
            catch (e) {
                console.log('error: ${e}')
            }
        }
    }

    return descriptor
}

class TestMethodDescriptor {

    @methodDescriptor
    testFunc(name) {
        console.log('func name: ${name}')
    }
}

let descriptorInstance = new TestMethodDescriptor()
descriptorInstance.testFunc('Giridhar')
descriptorInstance.testFunc('Reddy')


