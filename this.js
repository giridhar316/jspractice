
/*
   Arrow functions allows the lexical this (Refers to the this of the function it was created)
*/
var testFunc1 = function () {
    this.name = "ddd"

    this.innerFunc = function () {
        setTimeout(() => console.log(this), 1000)
     }
}

var inst = new testFunc1()
inst.innerFunc()

// Strict mode "this" return undefined, else returns "window" object or "global" for NODE
function f2() {
    'use strict'
    return this
}

f2() === undefined