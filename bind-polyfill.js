/*
    Polyfill for bind function
*/

if(!Function.prototype.bind) (function() {
    Function.prototype.bind = function() {
        let executingFunc = this
        let contextObject = arguments[0] //context object sent as first param to bind
        let args = Array.prototype.slice(arguments, 1) // ignores the context object

            // Function condition check can be also done using ES6 IsCallable
        if (typeof executingFunc !== 'function') {
            throw 'Not a function'
            // throw new TypeError('Invalid type, Not a valid function')
        }

        // returns the functiion that executes the actual function under the expected binding context when called.
        return function (){
            return executingFunc.apply(contextObject, args)
        }
    }
})()