function debounce(fn, timeToElapse) {
    let timer = null

    // closure func with access to timer
    return function() {
        let fnContext = this
        let fnArgs = arguments
        clearTimeout(timer)
        timer = setTimeout(()=> {
            fn.apply(fnContext, fnArgs)
        }, timeToElapse)
    }
}

let rootDiv = document.getElementById('footer')
let debounceTestFn = function () {
    console.log('fired')
}
rootDiv.addEventListener('onClick', debounce(debounceTestFn, 1000))
/*rootDiv.addEventListener('onClick', debounce(() => {
     console.log('fired')
    }, 2000))*/