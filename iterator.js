/*
   Custom Iterator
   Any object that implements a function with key name, "Symbol.iterator" becomes an iterator
   *
   ** Iterator of an object be invoked as  "" let test = fibonacciIterator[Symbol.iterator]() ""
   * Values can be read using (for item of iterator) OR using spread operator on the object instance as [...test]
*/
var fibonacciIterator = {
    [Symbol.iterator]() {
        let current = 1, prev=1
       
        return {
            next() {
                let retVal = prev
                prev = current
                current = retVal + current
                return {value: retVal, done: false }
            },

            return(currentValue) {
                return { value: currentValue, done: true}
            }
        }
    },

    testFunc: function () {
        console.log('Non iterator func')
    }
}

// test Iterator function's next implementation
let testNext = fibonacciIterator[Symbol.iterator]()
console.log(test.return())

// test Custom iterator
for (let item of fibonacciIterator) {
    console.log(item)
    if(item > 50) break
}