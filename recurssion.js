// write a function to recurssively loop through an array of alphabets, and sort them, and send a sorted destructured array back
//  *** Input can contain arrays within an array


function sortObject(input) {
    let output = []

    // self executing function to destructure the array input
   let destructureInput = function(itemArray) {
        for (let i=0; i<itemArray.length; i++) {
            let currentItem = itemArray[i]

            // if type is object, recurssively call the destructureInput function
            if (typeof currentItem === 'object') {
                destructureInput(currentItem)
            } else {
                output.push(currentItem)
            }
        }
    }

    destructureInput(input)
    return output.sort()
}

let input = ['d', 'c,', 'a', ['f', 'g', 'b'], 'k']
let output = sortObject(input)
console.log(output)


/* 
    Tail call recurssion 
*/
function factorial(n, aggregateSum=1) {
    if (n === 0) return aggregateSum

    /* Tail call recurssion. Calling the same function recurssively as the return value.
       Most modern JS engines can optimize the recurssion in this case, and resuse the same reference of the 
        callstack rather than creating a record on the call stack for each function call */
    return factorial(n-1, n * aggregateSum)
}
factorial(10)