/*
     
  Prototypal inheritance is the only inheritance applicable for JS, and Es6 class is just a syntactical sugar on top
     of prootypal inheritance with few classical inheritance concepts of Parent/CHild tight coupling. Classes do lose
       few prototype benefits that are available on regaular objects

  In prototypal inheritance, instances inherit from other instances unlike classical inheritance 
     where the conecpt of classes/sub-classes exist

    ** Prototypal inheritance can be classifies into 3 types.

    1) Concatinative inheritance - Using "Object.assign", we can make a new object inherit from old object,
           and extend the new object with newer functions. This is more or less COMPOSITION.
    
    2) Delegation Inheritance  - An object may have a link to a prototype for delegation, and all objects check for a func/object all the way
          upto root delegate. Delagtion refers to looking for a method/object all the way upto root, "Object.prototype".
          ** Constructor.prototype if hooked up when an object is instantitaed with "new" OR using "Object.create"

    3) Functional Inheritance - In JavaScript, any function can create an object. 
            When that function is not a constructor (or `class`), it’s called a factory function. 
            Functional inheritance works by producing an object from a factory, 
            and extending the produced object by assigning properties to it directly (using concatenative inheritance). 
        In JavaScript, any function can instantiate and return objects. 
            When you do so without a constructor, it’s called a factory function. 
            `class` can’t compete with the power and flexibility of factories — specifically stamps, and object pools are not the only factory use-case.

    **** Concatenative inheritance is the secret sauce that enables object composition in JavaScript.

    ** `Object.create()` is an ES5 feature that was championed by Douglas Crockford so that
             we could attach delegate prototypes without using constructors and the `new` keyword.

    ****NEW Keyword
    The `new` keyword is used to invoke a constructor. What it actually does is:
        Create a new instance
        Bind `this` to the new instance
        Reference the new object’s delegate [[Prototype]] to the object referenced by the constructor function’s `prototype` property.
        Reference the new object’s .constructor property to the constructor that was invoked.
        Names the object type after the constructor, which you’ll notice mostly in the debugging console. You’ll see `[Object Foo]`, for example, instead of `[Object object]`.
        Allows `instanceof` to check whether or not an object’s prototype reference is the same object referenced by the .prototype property of the constructor.
  */


  /* Delegate prototype, and composition combination */
  // Object literal
  let animal = {
    type: 'animal',

    describe() {
        console.log(`This is of type ${this.type} with ${this.legs} legs`)
    }
  }
  // Extended objects
  let dog = Object.assign(Object.create(animal), {
      type: 'dog',
      legs: 4
  })
  let cat = Object.assign(Object.create(animal), {
      type: 'cat',
      legs: 4
  })
  // Create a new scope func on the base object to test the prototype delegation
  animal.testScope = function() { console.log(`This is of type ${this.type}`)}
  // tests
  dog.describe()
  cat.describe()
  animal.testScope()
  dog.testScope()


  /* Factory Inheritance */
  let animal = {
    animalType: 'animal',
   
    describe () {
      return `An ${this.animalType} with ${this.furColor} fur, 
        ${this.legs} legs, and a ${this.tail} tail.`;
    }
  };
  let mouseFactory = function mouseFactory () {
    return Object.assign(Object.create(animal), {
      animalType: 'mouse',
      furColor: 'brown',
      legs: 4,
      tail: 'long, skinny'
    });
  };
  
  let mickey = mouseFactory();