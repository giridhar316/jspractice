// Sync retry func executor

function retry(fn, retryDelay=1000, maxAttempts=2) {
    // returns a function that executes the "fn", and takes in the arguments
    return function () {
        let attempts = 1
        let fnContext = this
        let fnArgs = arguments

        // Function executor
        let retryFuncExecutor = function (resolve, reject) {
            try
            {
                let result = fn.apply(fnContext, fnArgs)
                resolve(result)
            } catch (e) {
                console.log(`Failed attempts: ${attempts}`)
                if (attempts >= maxAttempts) {
                     reject(e)
                     return
                }
                setTimeout(() => retryFuncExecutor(resolve, reject), retryDelay)
                attempts += 1
            }
        }

        // returns a promise
        return new Promise((resolve, reject) => {
            retryFuncExecutor(resolve, reject)
        })
    }
}

// test retry logic with no failures
let retryFunc = retry((testInput) => {console.log(testInput)})
retryFunc('Giri').then(() => {
        console.log('success')
    },
    (error) => console.log('error')
).catch((e) => {console.log(e)}) /*catch errors from success resolve if any*/


// test retry logic with one failure
let retryFuncFailureCounter = 1
let retryFuncFailure = retry((testInput) => {
    if (retryFuncFailureCounter < 2) {
        retryFuncFailureCounter += 1
        throw 'error'
    } 
    console.log(testInput)
})
retryFuncFailure('N').then(() => {
        console.log('success')
    },
    (error) => console.log('error')
).catch((e) => {console.log(e)}) /*catch errors from success resolve if any*/