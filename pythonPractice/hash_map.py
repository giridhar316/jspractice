
from doubly_linked_list import DoublyLinkedList


class HashMap:
    """
    Simple hash map implementation in Python. Uses Doubly linked list and array to store the values.
    ** Array index matches the hash index, and array value holds the linked list reference.
    ** Handles collisons
    """

    HASH_STATUS = {
        'Added': 'Added',
        'Removed': 'Removed',
        'ExistingItem': 'ExistingItem',
        'MissingItem': 'MissingItem'
    }

    def __init__(self, max_size=2000):
        self.__max_size = max_size
        # use numpty package instead of this
        self.__hash_node_array = [None] * max_size


    def add(self, key, value):
        # TODO include a condition check to ensure array size does not go beyond max_size
        existing_node = self._get_hash_node(key)

        if existing_node is None:
            hash = self.__calculate_simple_hash(key)
            hash_index = self.__calculate_hash_index(hash)
            new_hash_item  = HashItem(key, value, hash_index)
            # add the new hash item as a value to a new doubly linked list, and insert it to the array at the hashindex
            self.__hash_node_array[hash_index] = DoublyLinkedList(new_hash_item)
            print ('added new node for key: ', key)
            return HashMap.HASH_STATUS['Added']
        else:
            # If node is already present, collision case arises, and next hash item is appended to the existing doubly linked list 
            head_node = existing_node.get_head_node()
            collision_node = self._find_collision_node(key, head_node)
            # if collsion_node is found, don't add duplicates
            if collision_node is not None:
                print('Key/Value pair already exists')
                return HashMap.HASH_STATUS['ExistingItem']

            new_hash_item  = HashItem(key, value, head_node.value.index)
            existing_node.add(new_hash_item)
            print ('added new collision node for key: ', key)
            return HashMap.HASH_STATUS['Added']
    

    def get(self, key):
        hash_node = self._get_hash_node(key)
        # pickup the head node of the linked list if applicable
        head_node = None if hash_node is None else hash_node.get_head_node()

        if head_node is not None and head_node.value.key == key:
            print ('Exiting key: value: ', key, head_node.value.value)
            return head_node.value.value
        elif head_node is None or head_node.next_node is None: # Non existent key
            print ('Non existent key: ', key, head_node)
            return None
        # get the value from the collision nodes
        collison_node = self._find_collision_node(key, head_node)
        return None if collison_node is None else collison_node.value.value


    def remove(self, key):
        hash_node = self._get_hash_node(key)
        # pickup the head node of the linked list if applicable
        head_node = None if hash_node is None else hash_node.get_head_node()

        if head_node is None:
            print ('no node with key ', key)
            return None

        if head_node.value.key == key:
            # self.__hash_node_array.pop(head_node.value.index)
            self.__hash_node_array[head_node.value.index] = None
            hash_node.delete(head_node)
            return head_node
        else:
            # collison
            collision_node = self._find_collision_node(key, head_node)
            if collision_node is not None:
                print ('Collision node found for removal: ', key)
                self.__hash_node_array[collision_node.value.index] = None
                hash_node.delete(collision_node)
                return collision_node
            return None
        

    def _get_hash_node(self, key):
        hash = self.__calculate_simple_hash(key)
        hash_index = self.__calculate_hash_index(hash)
        try:
            print ('_get_hash_node: hash_index: ', key, hash_index)
            print (len(self.__hash_node_array))
            return self.__hash_node_array[hash_index]
        except IndexError:
            print ('Index error')
            return None


    def _find_collision_node(self, key, current_node):
       collision_node = current_node
       # loop through the nodes, and find the node that matches the key
       while collision_node is not None and collision_node.value.key != key:
            collision_node = collision_node.next_node
       return collision_node


    def get_load_factor(self):
            curent_size = len(self.__hash_node_array)
            return curent_size/self.__max_size if curent_size > 0 else 0


    def __calculate_hash_index(self, hash):
        """ Hash index is dependent on the hash map size. 
        All hash values have to be calculated when rehashing happens
        """
        return hash % self.__max_size


    def __calculate_simple_hash(self, key):
        """
        Hash is independent of map size Simple hash that expects key to be a string
        Using a big prime to multiply is a better way of generating hash, and this is a simple test hash function
        """
        if key is None:
            raise 'Invalid key type'
        return ord(key[0]) + ord(key[1]) if len(key) > 1 else ord(key[0])



class HashItem():
    def __init__(self, key, value, index):
        self.key = key
        self.value = value
        self.index = index

def test_hash_map():
    test_hash_map_obj = HashMap()
    test_hash_map_obj.add('giri', 'reddy')
    test_hash_map_obj.add('test1', 'value')
    test_hash_map_obj.add('arjun', 'reddy')
    test_hash_map_obj.add('giri', 'duplicate')
    test_hash_map_obj.add('gir', 'collision')

    print test_hash_map_obj.get('giri')
    print test_hash_map_obj.get('ggggg')
    print test_hash_map_obj.get('giri')

    test_hash_map_obj.remove('giri')
    print test_hash_map_obj.get('gir')  
    test_hash_map_obj.remove('giri')
    print test_hash_map_obj.get('giri') 




