# find if a number a palindrome

def is_number_a_palindrome(number):
    #TODO check if input is a number
    if number < 11:
        return False

    input_string = str(number)
    input_string_length = len(input_string)

    # loop through the string array and compare if current digit matches with the relevant digit to the rear
    for i in range(input_string_length):
        j = i+1
        if input_string[i] != input_string[input_string_length - j]:
            return False

    return True

def test_palindrome():
    print is_number_a_palindrome(212)
    print is_number_a_palindrome(2111111112)
    print is_number_a_palindrome(214)
    print is_number_a_palindrome(22333322)