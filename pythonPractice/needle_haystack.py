# Find a needle in a haystack, AKA finding a substring in a string. O(n) is best case, and O(nm) is the worst case,
#    where n is the length of the string, and m is the length of the substring

def find_substring(input, search_string):
    """
        Find a substring(needle) in a string (haystack)
    """
    input_length = len(input)
    search_string_length = len(input)
    # O(nm)
    for index in range(input_length):
        match = False
        match_string = ''
        for search_index in range(search_string_length):
            # for each search char try to match the chars in the input until the entire search string matches
            # keep appending search chars 
            match_string += search_string[search_index]
            match = True if search_string[search_index] == input[index+search_index] else False
            # if non match at any point, OR match string equals search_string, break out of the second for loop
            if match is False or match_string == search_string:
                break
        # At any point, if match is true, return the object that holds the index, and the search string 
        print ('Match found: ', match)
        if match is True:
            return { 'start_index': index, 'match_string': search_string }
                
    return None
