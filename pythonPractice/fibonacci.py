# Fibonacci can be implemented by using dynamic programming (memoization to save space)

# **** Dynamic Programming is mainly an optimization over plain recursion. Wherever we see a recursive solution 
#   that has repeated calls for same inputs, we can optimize it using Dynamic Programming. 
#   The idea is to simply store the results of subproblems, so that we do not have to re-comupute them when 
#   needed later. This simple optimization reduces time complexities from exponential to polynomial.

#regular implementation
#
# brute force ineffective implemntation that does a lot of repeated work, and makes 
#   time complexity exponential (T(n) = T(n-1) + T(n-1)
def fibonacci_crude(n):
    if n <= 0:
        return 0
    elif n == 1:
        return 1

    return fibonacci_crude(n-1) + fibonacci_crude(n-2)


# better implementation
#
#  Tail call recurssion (function call memoization)
#    helps depending on the language support
def fibonacci_better(n, a=0, b=1):
    if n <= 0:
        return a
    elif n == 1:
        return b

    return fibonacci_better(n-1, b, a+b)


# best implementation
#
# dynamic programming (full blown memoization of function response)


print fibonacci_crude(9)
print fibonacci_better(9)
