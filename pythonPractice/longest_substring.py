'''
    Given a string, find the length of the longest substring without repeating characters.

    Example 1:
        Input: "abcabcbb"
        Output: 3 
        Explanation: The answer is "abc", with the length of 3. 

'''

def find_length_of_longest_substring(input):
    # TODO check for nulls/empty

    '''
        Implementation:  Maintain two counters, and loop through the input string, 
            and set the external counter until a dupe is found, reset the internal counter for every new check, and
            reset the external counter only when the internal counter is greater than external
            O(n)
    '''
    input_length = len(input)
    output_counter_length = 0
    reset_counter_length = 0
    reset_char_array = {}
    previous_char = ''


    for i in range(input_length):
        current_char = input[i]
        if (current_char != previous_char and not reset_char_array.has_key(current_char)):
            # if characters mismatch, and not a dupe then increment the reset counter, and increment the output counter if required
            reset_counter_length += 1
            reset_char_array[current_char] = current_char
            if reset_counter_length > output_counter_length:
                output_counter_length = reset_counter_length
        else:
            # reset to 1 for current element and start again
            reset_counter_length = 1
            reset_char_array = {}
            reset_char_array[current_char] = reset_char_array

        # reset prev character to current char
        previous_char = current_char

    print output_counter_length
    return output_counter_length


find_length_of_longest_substring('abcabcbb')
find_length_of_longest_substring('abcabcbbdefgh')




