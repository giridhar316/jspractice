'''
  Given a string input, create a function that will output a 
  compressed version of the string. E.g. Input = "AAABBZZDDD", Output = "A3B2Z2D3" 
'''

def compressStringByCharDupes(input):
    '''Two loops to figure out the count of characters.
        Reset the current index of the first loop based on the count of chars for the next char.
        O(n) time
    '''
    input_length = len(input)
    current_index = 0
    output = ''

    # check until index is less than the last element
    while current_index < input_length:
        current_char = input[current_index]
        current_char_count = 0

        # find matches of the current char from the current index, and increment the count if there is a match, else break
        for i in range(current_index, input_length):
            if input[i] == current_char:
                current_char_count += 1
                # reset to input length if last element to end the function
                if i == input_length-1:
                    current_index = input_length 
            else:
                current_index = i
                break

        # append the current char and count to output
        output += '{0}{1}'.format(current_char, current_char_count)

    print output
    return output


compressStringByCharDupes('AAABBZZDDDF')
compressStringByCharDupes('AAABBZZDDD')




