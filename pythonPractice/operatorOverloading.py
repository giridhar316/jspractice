class Vector:
    def __init__(self, a, b):
        self.a = a
        self.b = b


    def __str__(self):
        ''' Overload the base implementation '''
        return 'Vector (%d, %d)' % (self.a, self.b)

    def __add__(self, second):
        ''' Overloads the operator + implementation '''
        return Vector(self.a + second.a, self.b + second.b)

v1 = Vector(5, 2)
v2 = Vector(3, 5)

print v1 + v2
    