def function_request_logger(f):
    # wrapper
    def wrapper(*args, **kwargs):
        print 'Function call wrapper'
        # call the function
        f(*args, **kwargs)
    return wrapper

@function_request_logger
def test_decorator(param1, param2):
    print param1, param2

test_decorator('test1', 'test2')