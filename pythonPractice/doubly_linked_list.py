class DoublyLinkedList:
    """ Doubly linkedlist implementation (previous_node, value, next_node)
    """
    def __init__(self, value=None):
        # if previous_node is None, then it is the head node
        self.__head_node = LinkedListNode(value) 
        self.__current_node = self.__head_node


    def add(self, value):
        # create a new node
        new_node = LinkedListNode(value, self.__current_node)
        self.add_node(new_node)
        return new_node

    
    def add_node(self, node):
        # set the current node nextNode to the new node
        self.__current_node.next_node = node
        # reset current_node with new_node
        self.__current_node = node


    def delete(self, node):
        # if head node and more nodes are present, just reset the value to None, else remove the node
        if node.previous_node is None:
            if node.next_node is None:
                print ('Head node removed')
                node = None
            else:
                print ('Head node cannot be deleted. Just resetting the value')
                node.value = None
            return

        # reset the next_node ref of the previous_node to next_node of current_node
        node.previous_node.next_node = node.next_node
        # reset previous node of the next node if applicable
        if node.next_node is not None:
            node.next_node.previous_node = node.previous_node
        # reset current node if applicable
        if node == self.__current_node:
            self.__current_node = node.previous_node

        node = None # should be collected by GC


    def get_current_node(self):
        return self.__current_node

    
    def set_current_node(self, node):
        self.__current_node = node


    def get_head_node(self):
        return self.__head_node

class LinkedListNode():
    def __init__(self, value, previous_node=None, next_node=None):
        self.value = value
        self.previous_node = previous_node
        self.next_node = next_node