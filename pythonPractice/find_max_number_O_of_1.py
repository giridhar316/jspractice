# Design a special stack such that maximum element can be found in O(1) time and O(1) extra space

"""
 Logic is to insert the  maxvalue in a seperate variable (O(1)), and store a calculated value on the stack which can be
 used to trace back to the previous max value when an element is popped

 Push - max value related stack element calculation (2 * newMax - oldMax)
 Pop -  Previous max value calculation based on stack element value (2 * maxValue - stackElementValueToBePopped)

 
Push(x) : Inserts x at the top of stack.

If stack is empty, insert x into the stack and make maxEle equal to x.
If stack is not empty, compare x with maxEle. Two cases arise:
If x is less than or equal to maxEle, simply insert x.
If x is greater than maxEle, insert (2*x – maxEle) into the stack and make maxEle equal to x. 
  For example, let previous maxEle was 3. Now we want to insert 4. 
    We update maxEle as 4 and insert 2*4 – 3 = 5 into the stack.
Pop() : Removes an element from top of stack.

Remove element from top. Let the removed element be y. Two cases arise:
    If y is less than or equal to maxEle, the maximum element in the stack is still maxEle.
    If y is greater than maxEle, the maximum element now becomes (2*maxEle – y), so update (maxEle = 2*maxEle – y). 
    This is where we retrieve previous maximum from current maximum and its value in stack. For example, 
    let the element to be removed be 5 and maxEle be 4. We remove 5 and update maxEle as 2*4 – 5 = 3.

Important Points:


Stack doesn’t hold actual value of an element if it is maximum so far.
Actual maximum element is always stored in maxEle

"""

def find_max_in_a_stack():
    break