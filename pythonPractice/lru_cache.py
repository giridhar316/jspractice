
from hash_map import HashMap
from doubly_linked_list import DoublyLinkedList


class LruCache:
    """
     Theme of LRU cache is to remove least recently used frame when cache is full while a new item is being added

     FIFO

     *** To implement an LRU cache we need the following data structures.
     1) QUEUE - Can be implemented using doubly linked list (prevNode, value, nextNode) to hold the data.
     2) Hash - Map top hold the {data id - ex: employeeid, etc..} as the key, and the corresponding Queue node as the value.
    """
    def __init__(self, cache_max_size=100):
        self.__hash_map_obj = HashMap()
        self.__queue_obj = DoublyLinkedList(None)
        self.__current_size = 0
        self.__cache_max_size = cache_max_size

    
    def add(self, key, value):
        if self.__is_duplicate(key) is not True:
            # clear cache when full
            if self.__is_cache_full():
                print ('Cache is full, clearing space: {0}, CurrentSize: {1} ', key, self.__current_size)
                self.__clear_cache_space()

            new_cache_item = LruCacheItem(key, value)
            # tag the cache item to the queue
            new_node = self.__queue_obj.add(new_cache_item)
            # tag the queue node to hash object
            self.__hash_map_obj.add(key, new_node)
            self.__current_size += 1
            return new_cache_item
        return None


    def remove(self, key):
        # remove from the hash
        item_node = self.__hash_map_obj.remove(key)
        if item_node is not None:
            print ('removing item for key: ', key)
            # remove from the queue and reset the current size
            self.__queue_obj.delete(item_node)
            self.__current_size -=1
        return item_node
        

    def get(self, key):
        item_node = self.__hash_map_obj.get(key)
        current_node = self.__queue_obj.get_current_node()
        # if item node is not None and not the current node, then reset LRU, and push the accessednode to the last
        if item_node is not None and current_node != item_node:
            print ('pushing currently accessed node to the last', key)
            item_node.previous_node.next_node = item_node.next_node
            item_node.next_node.previous_node = item_node.previous_node
            # treat the accessed node as just pushed into the queue, and reset the current node on the queue
            item_node.next_node = None
            current_node.next_node = item_node
            item_node.previous_node = current_node
            self.__queue_obj.set_current_node(item_node)

        return item_node


    def __clear_cache_space(self):
        """
            Clears the least recently used item when cache is full.
        """
        head_node = self.__queue_obj.get_head_node()
        cache_item = head_node.value
        if cache_item is not None:
            self.__hash_map_obj.remove(cache_item.key)
            self.__current_size -= 1
            # Remove the first added item by removing the reference from the Queue by tagging the next item to head node
            head_node.next_node = head_node.next_node.next_node
            head_node.next_node.previous_node = head_node
            

    def __is_duplicate(self, key):
        return self.__hash_map_obj.get(key) != None


    def __is_cache_full(self):
        return self.__cache_max_size == self.__current_size

    
class LruCacheItem:
    def __init__(self, key, value):
        self.key = key
        self.value = value



def test_lru_cache():
    cache_obj = LruCache(5)
    cache_obj.add('test1', 'value1')
    cache_obj.add('test2', 'value2')
    cache_obj.add('test3', 'value3')
    cache_obj.add('test4', 'value4')
    cache_obj.add('test5', 'value5')

    cache_obj.get('test2')

    cache_obj.add('test6', 'value5')
    cache_obj.add('test7', 'value5')

    cache_obj.remove('test4')

    cache_obj.get('test4')






    
