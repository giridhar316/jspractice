1. /* maximum-k element sum from an array (Find the max sum of contiguos numbers in an array)

   *** kadane's algorithm.
   1. If all the numbers in the array are positive numbers, then sum of the entire array is the max array - o(n)
   2. If all the numbers in the array are negative numbers, then the least negative number is the max sum.

*/

function maxKElementSum(inputArray) {
    let positiveMax = 0
    let negativeMax
    let rollingMax = 0
    let allNegative = true // if all are negative, lowest negative number is the max sum

    inputArray.forEach((item) => {
        if (item < 0) {
            // negative, reset the rolling max to capture the next contiguos max number set
            rollingMax = 0

            if (allNegative) {
                // set the item value for the first time, and chek the max checkk from then on
               negativeMax = !negativeMax ? item : ((item > negativeMax) ? item : negativeMax)
            }
        } else {
            allNegative = false
            rollingMax = rollingMax + item
            if (rollingMax > positiveMax) {
                positiveMax = rollingMax
            }
        }
    })
    console.log(allNegative ? negativeMax : positiveMax)
    return allNegative ? negativeMax : positiveMax
}

maxKElementSum([9, 4, 5,-4, 6, 10, 9])
maxKElementSum([-4, -6, -1, -8, -9])
maxKElementSum([9, 4, 5, 6, 10, 9])